import datetime
import requests
import pprint
import copy
import json

# в словаре ниже собраны названия всех доменов на cs и ссылки на их прометеи
urlbI = {
   # "pnp-istio": "https://prometheus.pnp-istio-prod.yc.mvideo.ru/",
   # "pnp": "https://prometheus.pnp.yc.mvideo.ru/",
   # "mac": "https://prometheus.mac.yc.mvideo.ru/",       #  9NvqBLK7k  WL1SqGOnz
    "customer-domain": "https://prometheus.customer-domain.yc.mvideo.ru/",    #  pEPWjI5nk    U2fTQKHnz
   # "lards": "https://prometheus.lards.yc.mvideo.ru/",
   # "lastmile": "https://prometheus.lastmile.yc.mvideo.ru/",
   # "cs-prf": "https://prometheus.cs-prf.yc.mvideo.ru/",    #   NJxOAhK7z    oOAFqMOnk
   # "order-execution": "https://prometheus.order-execution.yc.mvideo.ru/",  # 6ScoM2Fnz  XDh03Mdnk
   # "order-placement": "https://prometheus.order-placement.yc.mvideo.ru/",
   # "tpyc": "https://prometheus.tp.yc.mvideo.ru/", 
   # "tp": "https://prometheus.tp.mvideo.ru/" #   PznHlbF7k
}


for url in urlbI:
    # запрос в prometheus
    response = requests.get(urlbI[url] + '/api/v1/query', params={'query': 'sum by (service, uri) (increase(http_server_requests_seconds_count{namespace=~".*prod.*", service!~".*uber.*", uri!~".*actuator.*|.*swagger.*|.*webjars.*|root|UNKNOWN|NOT_FOUND|.*camunda.*|/|.*api-docs.*|REDIRECTION", uri!="/**"}[7d]))'})
    # response = requests.get(urlbI[url] + '/api/v1/query', params={'query': 'sum by (service, uri) (increase(http_server_requests_seconds_count{service!~".*uber.*", uri!~".*actuator.*|.*swagger.*|.*webjars.*|root|UNKNOWN|NOT_FOUND|.*camunda.*|/|.*api-docs.*|REDIRECTION", uri!="/**"}[7d])) > 0'})
    # для тп
    # response = requests.get(urlbI[url] + '/api/v1/query', params={'query': 'sum by (app, uri) (increase(http_server_requests_seconds_count{namespace=~".*prod.*", app!~".*uber.*", uri!~".*actuator.*|.*swagger.*|.*webjars.*|root|UNKNOWN|NOT_FOUND|.*camunda.*|/|.*api-docs.*|REDIRECTION", uri!="/**"}[7d]))'})
    # для pnp-istio
    res = response.json()  # ответ от prometheus в формате json
    with open(url + '1.json', 'r', encoding='utf-8') as f:
        servicesanduri = json.load(f)  # запись в словарь данных из файла в формате json, в котором собрана вся информация от сервисах и точках домена

    for i in res['data']['result']:
        if i['metric']['service'] + " " + i['metric']['uri'] not in servicesanduri:
            key = i['metric']['service'] + " " + i['metric']['uri']
            servicesanduri[key] = {}
            servicesanduri[key]['4xx'] = 3
            servicesanduri[key]['5xx'] = 2
            servicesanduri[key]['time'] = 3
            servicesanduri[key]['description'] = "Приоритет: "
            servicesanduri[key]['priority2xx'] = "Днем10"
            servicesanduri[key]['priority4xx'] = "Днем10"
            servicesanduri[key]['priority5xx'] = "Срочный"
            servicesanduri[key]['prioritytime'] = "Днем10"
            # выше происходит запись в словарь дефолтных данных для сервисов и точек, которых не было в файле
    with open(url + '1.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(servicesanduri, ensure_ascii=False))  # запись словаря в json
    #print(servicesanduri)

    jsonstartdict = {
  "annotations": {
    "list": [
      {
        "builtIn": 1,
        "datasource": "-- Grafana --",
        "enable": True,
        "hide": True,
        "iconColor": "rgba(0, 211, 255, 1)",
        "name": "Annotations & Alerts",
        "target": {
          "limit": 100,
          "matchAny": False,
          "tags": [],
          "type": "dashboard"
        },
        "type": "dashboard"
      }
    ]
  },
  "editable": True,
  "gnetId": None,
  "graphTooltip": 0,
  "id": 48,
  "links": [],
  "panels": [],
  "refresh": False,
  "schemaVersion": 30,
  "style": "dark",
  "tags": [],
  "templating": {
    "list": []
  },
  "time": {
    "from": "now-1h",
    "to": "now"
  },
  "timepicker": {},
  "timezone": "",
  "title": "CS Alerts on Grafana",
  "uid": "Bfrf2p7znfid",
  "version": 1
}  # стартовый словарь для grafana

    alertdict = {
            "alertRuleTags": {},
            "conditions": [
              {
                "evaluator": {
                  "params": [],
                  "type": "gt"
                },
                "operator": {
                  "type": "or"
                },
                "query": {
                  "params": [
                    "A",
                    "1m",
                    "now"
                  ]
                },
                "reducer": {
                  "params": [],
                  "type": "last"
                },
                "type": "query"
              }
            ],
            "executionErrorState": "keep_state",
            "for": "3m",
            "frequency": "1m",
            "handler": 1,
            "message": "",
            "name": "2xx Запросы в точку: /api/v2/catalog-service/products () alert",
            "noDataState": "ok",
            "notifications": [
              # {
              #   "uid": "pEPWjI5nk"
              # },
              # {
              #   "uid": "U2fTQKHnz"
              # }
            ]
    }  # словарь для алерта в grafana

    paneldict = {  
          "alert": {},
          "aliasColors": {},
          "bars": False,
          "dashLength": 10,
          "dashes": False,
          "datasource": None,
          "description": "",
          "fill": 1,
          "fillGradient": 0,
          "gridPos": {
            "h": 9,
            "w": 6,
            "x": 0,
            "y": 1
          },
          "hiddenSeries": False,
          "id": 72,
          "legend": {
            "alignAsTable": True,
            "avg": True,
            "current": True,
            "max": True,
            "min": True,
            "show": True,
            "total": True,
            "values": True
          },
          "lines": True,
          "linewidth": 1,
          "nullPointMode": "null",
          "options": {
            "alertThreshold": True
          },
          "percentage": False,
          "pluginVersion": "8.1.3",
          "pointradius": 2,
          "points": False,
          "renderer": "flot",
          "seriesOverrides": [],
          "spaceLength": 10,
          "stack": False,
          "steppedLine": False,
          "targets": [
            {
              "exemplar": True,
              "expr": "sum by (container, uri)(increase(http_server_requests_seconds_count{status=~\"2..\", service=\"catalog-service\", uri=~\"/api/v2/catalog-service/products\", namespace=~\".*prod.*\"}[30s]))",
              "interval": "",
              "legendFormat": "2xx ошибки {{container}} {{uri}}",
              "refId": "A"
            }
          ],
          "thresholds": [],
          "timeFrom": None,
          "timeRegions": [],
          "timeShift": None,
          "title": "2xx Запросы в точку: /api/v2/catalog-service/products ()",
          "tooltip": {
            "shared": True,
            "sort": 0,
            "value_type": "individual"
          },
          "type": "graph",
          "xaxis": {
            "buckets": None,
            "mode": "time",
            "name": None,
            "show": True,
            "values": []
          },
          "yaxes": [
            {
              "$$hashKey": "object:873",
              "format": "short",
              "label": None,
              "logBase": 1,
              "max": None,
              "min": None,
              "show": True
            },
            {
              "$$hashKey": "object:874",
              "format": "short",
              "label": None,
              "logBase": 1,
              "max": None,
              "min": None,
              "show": True
            }
          ],
          "yaxis": {
            "align": False,
            "alignLevel": None
          }
        }  # словарь для панели в grafana

    deltaX = 6
    deltaY = 9
    # размеры панели по х и по у
    panels = []

    count1 = 0

    id = 2

    name = " alert"

    start1 = copy.deepcopy(jsonstartdict)

    for s in servicesanduri:
        for i in range(4):
            dict1 = copy.deepcopy(paneldict)
            dict1["alert"] = copy.deepcopy(alertdict)
            service_uri = s.split(" ")
            cx = count1 % 4
            cy = count1 // 4
            dict1["gridPos"]["x"] = deltaX * cx  # позиция панели по х
            dict1["gridPos"]["y"] = deltaY * cy + 1  # позиция панели по у
        

            curr_y = dict1["gridPos"]["y"]
    
            if cx == 0:
                dict1["id"] = id
                dict1["title"] = "2xx Запросы в точку: " + service_uri[1]
                dict1["alert"]["name"] = "2xx Запросы в точку: " + service_uri[1] + name
                #dict1["alert"]["conditions"].append(copy.deepcopy(condition_dict))
                #dict1["alert"]["conditions"][0]["evaluator"]["params"].append(0)
                dict1["alert"]["conditions"][0]["evaluator"]["type"] = "no_value"
                description = servicesanduri[s]["description"].replace("Приоритет: ", "Приоритет: Днем10")
                dict1["alert"]["message"] = "Описание события:  \"Отстутствие 2xx запросов\"\n" + description
                # dict1["alert"]["message"] = "Домен: " + url + "\nСервис: " + service_uri[0] + "\nEndpoint: " + service_uri[1] + "\nСпецификация: \nПотребители: \nВнешние источники: \nОписание события:  \"Отстутствие 2xx запросов\"\nПриоритет: Днем10\nДействия ДС: Оповестить L2 CS"
                #dict1["targets"].append(copy.deepcopy(target_dict))
                dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"2..\", service=\"{service}\", uri=~\"{uri}\", namespace=~\".*prod.*\"}}[30s]))".format(service=service_uri[0], uri=service_uri[1])
                #dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"2..\", service=\"{service}\", uri=~\"{uri}\"}}[30s]))".format(service=s, uri=u)
                dict1["targets"][0]["legendFormat"] = "2xx ошибки {{container}} {{uri}}"
                #dict1["thresholds"][0]["value"] = 0
                panels.append(dict1)
                count1 += 1
                id += 1
                # выше формируется часть json для grafana, связанная с 2хх запросами
            elif cx == 1:
    
                dict1["id"] = id
                dict1["title"] = "4xx Запросы в точку: " + service_uri[1]
                dict1["alert"]["name"] = "4xx Запросы в точку: " + service_uri[1] + name
                #dict1["alert"]["conditions"].append(copy.deepcopy(condition_dict))
                dict1["alert"]["conditions"][0]["evaluator"]["params"].append(servicesanduri[s]["4xx"])
                description = servicesanduri[s]["description"].replace("Приоритет: ", "Приоритет: " + servicesanduri[s]["priority4xx"])
                dict1["alert"]["message"] = "Описание события:  \"Превышение кол-ва 4xx ошибок\"\n" + description
                # dict1["alert"]["message"] = "Домен: " + url + "\nСервис: " + service_uri[0] + "\nEndpoint: " + service_uri[1] + "\nСпецификация: \nПотребители: \nВнешние источники: \nОписание события:  \"Превышение кол-ва 4xx ошибок\"\nПриоритет: Днем10\nДействия ДС: Оповестить L2 CS"
                #dict1["targets"].append(copy.deepcopy(target_dict))
                dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"4..\", service=\"{service}\", uri=~\"{uri}\", namespace=~\".*prod.*\"}}[30s]))".format(service=service_uri[0], uri=service_uri[1])
                #dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"4..\", service=\"{service}\", uri=~\"{uri}\"}}[30s]))".format(service=s, uri=u)
                dict1["targets"][0]["legendFormat"] = "4xx ошибки {{container}} {{uri}}"
                #dict1["thresholds"][0]["value"] = 0
                panels.append(dict1)
                count1 += 1
                id += 1
                # выше формируется часть json для grafana, связанная с 4хх ошибками
            elif cx == 2:
    
                dict1["id"] = id
                dict1["title"] = "5xx Запросы в точку: " + service_uri[1]
                dict1["alert"]["name"] = "5xx Запросы в точку: " + service_uri[1] + name
                #dict1["alert"]["conditions"].append(copy.deepcopy(condition_dict))
                dict1["alert"]["conditions"][0]["evaluator"]["params"].append(servicesanduri[s]["5xx"])
                description = servicesanduri[s]["description"].replace("Приоритет: ", "Приоритет: " + servicesanduri[s]["priority5xx"])
                dict1["alert"]["message"] = "Описание события:  \"Превышение кол-ва 5xx ошибок\"\n" + description
                # dict1["alert"]["message"] = "Домен: " + url + "\nСервис: " + service_uri[0] + "\nEndpoint: " + service_uri[1] + "\nСпецификация: \nПотребители: \nВнешние источники: \nОписание события:  \"Превышение кол-ва 5xx ошибок\"\nПриоритет: Срочный\nДействия ДС: Оповестить L2 CS"
                #dict1["targets"].append(copy.deepcopy(target_dict))
                dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"5..\", service=\"{service}\", uri=~\"{uri}\", namespace=~\".*prod.*\"}}[30s]))".format(service=service_uri[0], uri=service_uri[1])
                #dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_count{{status=~\"5..\", service=\"{service}\", uri=~\"{uri}\"}}[30s]))".format(service=s, uri=u)
                dict1["targets"][0]["legendFormat"] = "5xx ошибки {{container}} {{uri}}"
                #dict1["thresholds"][0]["value"] = 0
                panels.append(dict1)
                count1 += 1
                id += 1
                # выше формируется часть json для grafana, связанная с 5хх ошибками
            elif cx == 3:
    
                dict1["id"] = id
                dict1["title"] = "Время ответа точки: " + service_uri[1]
                dict1["alert"]["name"] = "Время ответа точки: " + service_uri[1] + name
                #dict1["alert"]["conditions"].append(copy.deepcopy(condition_dict))
                dict1["nullPointMode"] = "connected"
                dict1["alert"]["conditions"][0]["evaluator"]["params"].append(servicesanduri[s]["time"])
                description = servicesanduri[s]["description"].replace("Приоритет: ", "Приоритет: " + servicesanduri[s]["prioritytime"])
                dict1["alert"]["message"] = "Описание события:  \"Превышение времени ответа\"\n" + description
                # dict1["alert"]["message"] = "Домен: " + url + "\nСервис: " + service_uri[0] + "\nEndpoint: " + service_uri[1] + "\nСпецификация: \nПотребители: \nВнешние источники: \nОписание события:  \"Превышение времени ответа\"\nПриоритет: Днем10\nДействия ДС: Оповестить L2 CS"
                #dict1["targets"].append(copy.deepcopy(target_dict))
                dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_sum{{service=\"{service}\", uri=~\"{uri}\", namespace=~\".*prod.*\"}}[1m])) / sum by (container, uri)(increase(http_server_requests_seconds_count{{service=\"{service}\", uri=~\"{uri}\", namespace=~\".*prod.*\"}}[1m]))".format(service=service_uri[0], uri=service_uri[1])
                #dict1["targets"][0]["expr"] = "sum by (container, uri)(increase(http_server_requests_seconds_sum{{service=\"{service}\", uri=~\"{uri}\"}}[1m])) / sum by (container, uri)(increase(http_server_requests_seconds_count{{service=\"{service}\", uri=~\"{uri}\"}}[1m]))".format(service=s, uri=u)
                dict1["targets"][0]["legendFormat"] = "Среднее время ответа {{container}} {{uri}}"
                #dict1["thresholds"][0]["value"] = 0
                panels.append(dict1)
                count1 += 1
                id += 1
                # выше формируется часть json для grafana, связанная со средним временем ответа
    start1["panels"] = panels
    resp = json.dumps(start1)
    with open('dict_' + url + '.json', 'w') as f:
        f.write(resp)  # выше происходит запись словаря в json для grafana
